"""Try to plot input file."""
from sys import argv, exit

from bcar import scenario

if __name__ == "__main__":
    if len(argv) == 2:
        # plot scenario
        try:
            sc = scenario.load(argv[1])
            scenario.plot(sc)
        except:
            print("Bad input JSON file.")
    else:
        print("Usage: python3 plot.py FILENAME")
        print()
        print("Where FILENAME is JSON file.")
        exit(1)
