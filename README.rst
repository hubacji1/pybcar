PyBCar
======

Python3 library with some geometric bicycle car computations.

License
-------

The project is published under `MIT License`_.

.. _MIT License: ./LICENSE
