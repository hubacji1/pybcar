"""Generate simple JSON scenario.

The scenario contains at least:

- `init` -- the init car position,

- `slot` -- the parking slot,

- `obst` -- the list of (convex polygon) obstacles.
"""
from json import dumps, loads
from math import cos, pi, sin
from random import random, uniform

from bcar import bcar

W = bcar.WIDTH
L = bcar.LENGTH
WB = bcar.WHEELBASE
CTC = bcar.CTC
MTR = bcar.MTR

SLOT_RADI = 20
OBST_L = 4
OBST_W = 2
OBST_COUNT = 8

def gen_init():
    """Generate car init position."""
    # TODO if changed, change ``gen_slot`` accordingly
    return (0, 0, 0)

def gen_slot(l=5.3, w=2.4):
    """Generate parking slot."""
    parallel = True if random() < 0.5 else False
    if parallel and l == 5.3 and w == 2.4:
        l = 6.5
        w = 2.2
    elif parallel:
        ol = l
        l = w
        w = ol
    right = 1.0 if random() < 0.5 else -1.0
    r = SLOT_RADI
    angl = uniform(0, 2 * pi)
    x = r * cos(angl)
    y = r * sin(angl)
    h = uniform(0, 2 * pi)
    if parallel:
        return [[
            [
                x + l/2 * cos(h - pi/2 * right),
                y + l/2 * sin(h - pi/2 * right),
            ],
            [
                x + w * cos(h) + l/2 * cos(h - pi/2 * right),
                y + w * sin(h) + l/2 * sin(h - pi/2 * right),
            ],
            [
                x + w * cos(h) + l/2 * cos(h + pi/2 * right),
                y + w * sin(h) + l/2 * sin(h + pi/2 * right),
            ],
            [
                x + l/2 * cos(h + pi/2 * right),
                y + l/2 * sin(h + pi/2 * right),
            ],
        ]]
    else:
        return [[
            [
                x + w/2 * cos(h - pi/2 * right),
                y + w/2 * sin(h - pi/2 * right),
            ],
            [
                x + l * cos(h) + w/2 * cos(h - pi/2 * right),
                y + l * sin(h) + w/2 * sin(h - pi/2 * right),
            ],
            [
                x + l * cos(h) + w/2 * cos(h + pi/2 * right),
                y + l * sin(h) + w/2 * sin(h + pi/2 * right),
            ],
            [
                x + w/2 * cos(h + pi/2 * right),
                y + w/2 * sin(h + pi/2 * right),
            ],
        ]]

def gen_obst():
    """Generate obstacles array."""
    obstacles = []
    min_r = ((W/2)**2 + ((WB+L)/2)**2)**0.5 + (OBST_W**2 + OBST_L**2)**0.5 / 2
    for i in range(OBST_COUNT):
        l = OBST_L
        w = OBST_W
        angl = uniform(0, 2 * pi)
        r = uniform(min_r**2, (SLOT_RADI - 5)**2)**0.5
        x = r * cos(angl)
        y = r * sin(angl)
        h = uniform(0, 2 * pi)
        obstacles.append([
            [
                x + w/2 * cos(h - pi/2) + l/2 * cos(h),
                y + w/2 * sin(h - pi/2) + l/2 * sin(h),
            ],
            [
                x + w/2 * cos(h - pi/2) - l/2 * cos(h),
                y + w/2 * sin(h - pi/2) - l/2 * sin(h),
            ],
            [
                x + w/2 * cos(h + pi/2) - l/2 * cos(h),
                y + w/2 * sin(h + pi/2) - l/2 * sin(h),
            ],
            [
                x + w/2 * cos(h + pi/2) + l/2 * cos(h),
                y + w/2 * sin(h + pi/2) + l/2 * sin(h),
            ],
            [
                x + w/2 * cos(h - pi/2) + l/2 * cos(h),
                y + w/2 * sin(h - pi/2) + l/2 * sin(h),
            ],
        ])
    return obstacles

if __name__ == "__main__":
    init = gen_init()
    slot = gen_slot()
    obst = gen_obst()
    print(dumps({
        "init": init,
        "slot": slot,
        "obst": obst,
    }))
