Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_ and this project adheres to
`Semantic Versioning`_.

.. _Keep a Changelog: http://keepachangelog.com/
.. _Semantic Versioning: http://semver.org/

`Unreleased`_
-------------

Added
^^^^^

- Intersection function.

`0.2.0` - 2020-03-13
--------------------

Added
^^^^^

- Scenario related functions.

- Generate simple scenario script.

0.1.0 - 2020-03-12
------------------

Added
^^^^^

- Changelog, license, readme.

- Basic bicycle car computations.

.. _Unreleased: https://gitlab.fel.cvut.cz/hubacji1/pybcar/compare/v0.2.0...master
.. _0.2.0: https://gitlab.fel.cvut.cz/hubacji1/pybcar/compare/v0.1.0...v0.2.0
