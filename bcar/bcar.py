"""Bicycle car functions."""
from math import asin, atan, cos, pi, sin, tan
from matplotlib import pyplot as plt

# Porsche Panamera 971 dimensions (default)
# see https://www.porsche.com/international/models/panamera/panamera-turbo-models/panamera-turbo/featuresandspecs/
LENGTH = 5.049
WIDTH = 2.165
HEIGHT = 1.427
WHEELBASE = 2.950

# found or measured
CTC = 11.8872 # curb-to-curb
DR = 1.0625 # distance from center to rear
DF = (lambda: LENGTH - DR)()

# derived constants
MTR = (lambda: ((CTC / 2)**2 - WHEELBASE**2)**0.5 - WIDTH / 2)()
MAX_STEER = (lambda: atan(WHEELBASE / MTR))()
INNER_RADI = (lambda: MTR - WIDTH / 2)()
OUTER_FRONT_RADI = (lambda: ((MTR + WIDTH / 2)**2 + DF**2)**0.5)()
OUTER_REAR_RADI = (lambda: ((MTR + WIDTH / 2)**2 + DR**2)**0.5)()

def ccl(pose):
    """Return circle center on the left side of a car.

    Keyword arguments:
    pose -- The pose of a car.
    """
    return (
        pose[0] + MTR * cos(pose[2] + pi / 2),
        pose[1] + MTR * sin(pose[2] + pi / 2),
    )

def ccr(pose):
    """Return circle center on the right side of a car.

    Keyword arguments:
    pose -- The pose of a car.
    """
    return (
        pose[0] + MTR * cos(pose[2] - pi / 2),
        pose[1] + MTR * sin(pose[2] - pi / 2),
    )

def lfx(pose):
    """Return left front ``x`` coordinate.

    Keyword arguments:
    pose -- The pose of a car.
    """
    lfx = pose[0]
    lfx += (WIDTH / 2.0) * cos(pose[2] + pi / 2.0)
    lfx += DF * cos(pose[2])
    return lfx

def lrx(pose):
    """Return left front ``x`` coordinate.

    Keyword arguments:
    pose -- The pose of a car.
    """
    lrx = pose[0]
    lrx += (WIDTH / 2.0) * cos(pose[2] + pi / 2.0)
    lrx += -DR * cos(pose[2])
    return lrx

def rrx(pose):
    """Return left front ``x`` coordinate.

    Keyword arguments:
    pose -- The pose of a car.
    """
    rrx = pose[0]
    rrx += (WIDTH / 2.0) * cos(pose[2] - pi / 2.0)
    rrx += -DR * cos(pose[2])
    return rrx

def rfx(pose):
    """Return left front ``x`` coordinate.

    Keyword arguments:
    pose -- The pose of a car.
    """
    rfx = pose[0]
    rfx += (WIDTH / 2.0) * cos(pose[2] - pi / 2.0)
    rfx += DF * cos(pose[2])
    return rfx

def lfy(pose):
    """Return left front ``y`` coordinate.

    Keyword arguments:
    pose -- The pose of a car.
    """
    lfy = pose[1]
    lfy += (WIDTH / 2.0) * sin(pose[2] + pi / 2.0)
    lfy += DF * sin(pose[2])
    return lfy

def lry(pose):
    """Return left front ``y`` coordinate.

    Keyword arguments:
    pose -- The pose of a car.
    """
    lry = pose[1]
    lry += (WIDTH / 2.0) * sin(pose[2] + pi / 2.0)
    lry += -DR * sin(pose[2])
    return lry

def rry(pose):
    """Return left front ``y`` coordinate.

    Keyword arguments:
    pose -- The pose of a car.
    """
    rry = pose[1]
    rry += (WIDTH / 2.0) * sin(pose[2] - pi / 2.0)
    rry += -DR * sin(pose[2])
    return rry

def rfy(pose):
    """Return left front ``y`` coordinate.

    Keyword arguments:
    pose -- The pose of a car.
    """
    rfy = pose[1]
    rfy += (WIDTH / 2.0) * sin(pose[2] - pi / 2.0)
    rfy += DF * sin(pose[2])
    return rfy

def lf(pose):
    """Return left front coordinates.

    Keyword arguments:
    pose -- The pose of a car.
    """
    return (lfx(pose), lfy(pose))

def lr(pose):
    """Return left rear coordinates.

    Keyword arguments:
    pose -- The pose of a car.
    """
    return (lrx(pose), lry(pose))

def rr(pose):
    """Return right rear coordinates.

    Keyword arguments:
    pose -- The pose of a car.
    """
    return (rrx(pose), rry(pose))

def rf(pose):
    """Return right front coordinates.

    Keyword arguments:
    pose -- The pose of a car.
    """
    return (rfx(pose), rfy(pose))

def car_frame(pose):
    """Return cars lines segments of frame without the front one to detect the
    direction of the car.

    Keyword arguments:
    pose -- The pose of a car.
    """
    l = []
    l.append((lf(pose), lr(pose)))
    l.append((lr(pose), rr(pose)))
    l.append((rr(pose), rf(pose)))
    return l

def car_lines(pose):
    """Return cars line segments of frame.

    Keyword arguments:
    pose -- The pose of a car.
    """
    l = []
    l.append((lf(pose), lr(pose)))
    l.append((lr(pose), rr(pose)))
    l.append((rr(pose), rf(pose)))
    l.append((rf(pose), lf(pose)))
    return l

# computing
def perfect_parking_slot_len():
    """Compute perfect parking slot length.

    see Simon R. Blackburn *The Geometry of Perfect Parking*
    see https://www.ma.rhul.ac.uk/SRBparking
    """
    r = CTC / 2
    l = WHEELBASE
    k = DF - WHEELBASE
    w = WIDTH
    return (
        LENGTH
        + (
            (r**2 - l**2)
            + (l + k)**2
            - ((r*r - l*l)**0.5 - w)**2
        )**0.5
        - l
        - k
    )

def next(pose, ctrl={}):
    """Return next position of ``pose`` car controlled by ``ctrl``.

    Keyword arguments:
    pose -- The pose of a car.
    ctrl -- The control of the car (speed, steer).
    """
    if "speed" in ctrl:
        speed = ctrl["speed"]
    else:
        speed = 1
    if "steer" in ctrl:
        steer = ctrl["steer"]
    else:
        steer = 0
    dx = speed * cos(pose[2])
    dy = speed * sin(pose[2])
    dh = speed / WHEELBASE * tan(steer)
    return (pose[0] + dx, pose[1] + dy, pose[2] + dh)

def rotate(pose, center=(0, 0), angl=0):
    """Rotate car ``pose`` with ``center`` by ``angle``.

    Keyword arguments.
    pose -- The pose of a car.
    center -- Coordinates of rotation center.
    angle -- How much to rotate.
    """
    px = pose[0]
    py = pose[1]
    (cx, cy) = center
    px -= cx;
    py -= cy;
    nx = px * cos(angl) - py * sin(angl);
    ny = px * sin(angl) + py * cos(angl);
    return (nx + cx, ny + cy, pose[2] + angl)

def intersection(fl, sl, segment=False):
    """Return coordinates of two lines intersection.

    See https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection

    Keyword arguments:
    fl -- The first line in ``(init, goal)`` format where ``init`` and ``goal``
          are 2D coordinates ``(x, y)``.
    sl -- The second line in ``(init, goal)`` format where ``init`` and
          ``goal`` are 2D coordinates ``(x, y)``.
    segment -- ``True`` if only line segment considered, ``False`` by default.
    """
    ((x1, y1), (x2, y2)) = fl if fl is not None else ((0, 0), (0, 0))
    ((x3, y3), (x4, y4)) = sl if sl is not None else ((0, 0), (0, 0))
    deno = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
    if deno == 0:
        return False
    if not segment:
        px = (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)
        px /= deno
        py = (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)
        py /= deno
        return (px, py)
    else:
        t = (x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)
        t /= deno
        u = (x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)
        u *= -1
        u /= deno
        if t < 0 or t > 1 or u < 0 or u > 1:
            return False
        return (x1 + t * (x2 - x1), y1 + t * (y2 - y1))
    return False

# plotting
def frame(lss):
    """Return `xcoords`, `ycoords` composed from `lines`.

    Keyword arguments:
    lss -- The array of line segments.
    """
    xcoords = []
    ycoords = []
    for ls in lss:
        xcoords.append(ls[0][0])
        ycoords.append(ls[0][1])
    xcoords.append(ls[1][0])
    ycoords.append(ls[1][1])
    return (xcoords, ycoords)

def plot(pose, obst=[], c={}):
    """Plot car.

    Keyword arguments:
    pose -- The pose of the car.
    obst -- Line segments as obstacles.
    c -- Colors:
        - car: car frame
        - drive: drivable directions
        - in: inner radius
        - of: outer front radius
        - or: outer rear radius
    """
    plt.axis("equal")
    plt.gca().set_xlim((-10, 10))
    #plt.gca().set_ylim((-10, 10))
    if len(obst) > 0:
        plt.plot(*frame(obst))
    if "car" in c:
        plt.plot(*frame(car_frame(pose)), color=c["car"])
    else:
        plt.plot(*frame(car_frame(pose)))

    ## uncomment to plot possible future drive directions
    #if "drive" in c:
    #    lc = plt.Circle(ccl(pose), MTR, color=c["drive"], fill=False)
    #    rc = plt.Circle(ccr(pose), MTR, color=c["drive"], fill=False)
    #else:
    #    lc = plt.Circle(ccl(pose), MTR, fill=False)
    #    rc = plt.Circle(ccr(pose), MTR, fill=False)
    #plt.gcf().gca().add_artist(lc)
    #plt.gcf().gca().add_artist(rc)

    ## uncomment to plot inner radius
    #if "in" in c:
    #    lc = plt.Circle(ccl(pose), INNER_RADI, color=c["in"], fill=False)
    #    rc = plt.Circle(ccr(pose), INNER_RADI, color=c["in"], fill=False)
    #else:
    #    lc = plt.Circle(ccl(pose), INNER_RADI, fill=False)
    #    rc = plt.Circle(ccr(pose), INNER_RADI, fill=False)
    #plt.gcf().gca().add_artist(lc)
    #plt.gcf().gca().add_artist(rc)

    ## uncomment to plot outer front radius
    #if "of" in c:
    #    lc = plt.Circle(ccl(pose), OUTER_FRONT_RADI, color=c["of"], fill=False)
    #    rc = plt.Circle(ccr(pose), OUTER_FRONT_RADI, color=c["of"], fill=False)
    #else:
    #    lc = plt.Circle(ccl(pose), OUTER_FRONT_RADI, fill=False)
    #    rc = plt.Circle(ccr(pose), OUTER_FRONT_RADI, fill=False)
    #plt.gcf().gca().add_artist(lc)
    #plt.gcf().gca().add_artist(rc)

    ## uncomment to plot outer rear radius
    #if "or" in c:
    #    lc = plt.Circle(ccl(pose), OUTER_REAR_RADI, color=c["or"], fill=False)
    #    rc = plt.Circle(ccr(pose), OUTER_REAR_RADI, color=c["or"], fill=False)
    #else:
    #    lc = plt.Circle(ccl(pose), OUTER_REAR_RADI, fill=False)
    #    rc = plt.Circle(ccr(pose), OUTER_REAR_RADI, fill=False)
    #plt.gcf().gca().add_artist(lc)
    #plt.gcf().gca().add_artist(rc)

    plt.show()
