"""Plot JSON formatted scenario."""
from json import loads
from matplotlib import pyplot as plt

from bcar import bcar

def load(fname="sc.json"):
    """Load scenario from file."""
    if fname is None:
        raise ValueError("File name as argument needed")
    with open(fname, "r") as f:
        scenario = loads(f.read())
    return scenario

def plot_nodes(nodes=[]):
    """Return ``xcoords``, ``ycoords`` arrays of nodes to plot.

    Keyword arguments:
    nodes -- The list of nodes to plot.
    """
    xcoords = []
    ycoords = []
    for n in nodes:
        xcoords.append(n[0])
        ycoords.append(n[1])
    return (xcoords, ycoords)

def plot(scenario):
    """Plot scenario.

    Keyword arguments:
    scenario -- The scenario to plot.
    """
    plt.rcParams["font.size"] = 24
    plt.rcParams['hatch.linewidth'] = 1.0
    fig = plt.figure()

    ax = fig.add_subplot(111)
    ax.set_aspect("equal")
    if "name" in scenario:
            ax.set_title("{}".format(scenanario["name"]))
    ax.set_xlabel("x [m]")
    ax.set_ylabel("y [m]")

    if "nodes_x" in scenario and "nodes_y" in scenario:
        plt.plot(
            scenario["nodes_x"],
            scenario["nodes_y"],
            color="lightgray",
            marker="o",
            ms=2,
            lw=0,
        )
    if "obst" in scenario and  len(scenario["obst"]) > 0:
        for o in scenario["obst"]:
            plt.plot(*plot_nodes(o), color="blue")
    if "path" in scenario and  len(scenario["path"]) > 0:
        for n in scenario["path"]:
            plt.plot(*bcar.frame(bcar.car_frame(n)), color="peachpuff")
        plt.plot(*plot_nodes(scenario["path"]), color="orange")
    if "slot" in scenario and len(scenario["slot"]) > 0:
        for s in scenario["slot"]:
            plt.plot(*plot_nodes(s), color="gray")
    if "init" in scenario and len(scenario["init"]) == 3:
        plt.plot(*bcar.frame(bcar.car_frame(scenario["init"])), color="red")
        plt.plot(
            scenario["init"][0],
            scenario["init"][1],
            color="red",
            marker="+",
            ms=12
        )
    if "goals" in scenario:
        for i in scenario["goals"]:
            if len(i) == 3:
                plt.plot(*bcar.frame(bcar.car_frame(i)), color="darkseagreen")
                plt.plot(i[0], i[1], color="darkseagreen", marker="+", ms=12)
    if "goal" in scenario and len(scenario["goal"]) == 3:
        plt.plot(*bcar.frame(bcar.car_frame(scenario["goal"])), color="green")
        plt.plot(
            scenario["goal"][0],
            scenario["goal"][1],
            color="green",
            marker="+",
            ms=12
        )
    if "starts" in scenario and len(scenario["starts"]) > 0:
        print("possible starts:")
        for p in scenario["starts"]:
            plt.plot(*p, color="red", marker="+", ms=12)
            print(" {}".format(p))

    handles, labels = ax.get_legend_handles_labels()
    plt.show()
